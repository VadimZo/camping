// АНИМАЦИЯ
AOS.init({
  disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean
  startEvent: 'DOMContentLoaded',
  offset: 200,
  //delay: 0,
  duration: 500,
  anchorPlacement: 'top-bottom',
});



// Уменьшение шапки при скроле
window.onscroll = function() {scrollFunction()};
function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.querySelector(".header_top").classList.add("min");
  } else {
    document.querySelector(".header_top").classList.remove("min");
  }
};




// Плавный скрол к ID
document.querySelectorAll('a[href^="#"').forEach(link => {
  link.addEventListener('click', function(e) {
      e.preventDefault();
      let href = this.getAttribute('href').substring(1);
      const scrollTarget = document.getElementById(href);
      // const topOffset = document.querySelector('.scrollto').offsetHeight;
      const topOffset = 0; // если не нужен отступ сверху
      const elementPosition = scrollTarget.getBoundingClientRect().top;
      const offsetPosition = elementPosition - topOffset;
      window.scrollBy({
          top: offsetPosition,
          behavior: 'smooth'
      });
  });
});



// Мобильное меню
const logoMobile=document.querySelector(".header_logo-mobile");
const headerTop = document.querySelector('.header_top');
const btnMobile = document.querySelector('.btn_mobile');
const headerContacts = document.querySelector('.header_nav');
const bodyLock = document.querySelector('body');
btnMobile.addEventListener('click', function() {
  headerTop.classList.toggle('_mobile');
  headerContacts.classList.toggle('_mobile');
  bodyLock.classList.toggle('bodyLock');
  logoMobile.style.display = logoMobile.style.display === 'none' ? '' : 'none';

});




// SLIDER ZERO
var mySwiper = new Swiper('.js-sl-zero', {
  loop: true,
  slidesPerView: 1,
  autoplay: {
    delay: 5000,
  },
  // navigation: {
  //   nextEl: '.swiper-zero-button-next',
  //   prevEl: '.swiper-zero-button-prev',
  // },
  pagination: {
    el: '.swiper-pagination_zero',
    clickable: true,
  },
});




// SLIDER river
var swiper = new Swiper(".js-sl-thumbs", {
  // spaceBetween: 10,
  slidesPerView: 5,
  freeMode: true,
  // watchSlidesProgress: true,
  autoplay: {
    delay: 4000,
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
    },
    400: {
      slidesPerView: 1.5,
    },
    600: {
      slidesPerView: 3,
    },
    750: {
      slidesPerView: 4,
    },
    1921: {
    },
  },
});
var mySwiper = new Swiper('.js-sl-river', {
  // loop: true,
  slidesPerView: 1,
  // autoplay: {
  //   delay: 10000,
  // },
  thumbs: {
    swiper: swiper,
  },
  navigation: {
    nextEl: '.swiper-river-button-next',
    prevEl: '.swiper-river-button-prev',
  },
});



// Маска телефона
let selector = document.querySelectorAll('input[type="tel"]');
var im = new Inputmask('+7(999) 999-99-99');
im.mask(selector);



// Scroll TOP
const offset = 200;
const scrollUp = document.querySelector('.btn__top');
const getTop = () => window.pageYOffset || document.documentElement.scrollTop;
window.addEventListener('scroll', () => {
  if (getTop() > offset) {
    scrollUp.classList.add('btn__top--active')
  } else {
    scrollUp.classList.remove('btn__top--active')
  }
});
scrollUp.addEventListener('click', () => {
  window.scrollTo({
    top: 0,
    behavior: 'smooth'
  });
});

document.addEventListener("DOMContentLoaded", function(event) {
  let prevRatio = 0.0;
  let element = document.querySelector('.scroll_filter');
  let observer = new IntersectionObserver(onEntry, {threshold: ThresholdArr()});

  function ThresholdArr(){
    let thresholds = []
    let steps = 5

    for (let i = 1.0; i <= steps; i++) {
      let ratio = i / steps
      thresholds.push(ratio)
    }
    return thresholds
  }

  function onEntry(entry) {
    let ent=entry[0];
    let curRatio = ent.intersectionRatio;

    curRatio > prevRatio ? ent.target.style.filter = `grayscale(${1-curRatio})` : ent.target.style.filter = `grayscale(${1-curRatio})`
    console.log(curRatio);
    prevRatio = curRatio

    if(!ent.isIntersecting){ent.target.style.filter='grayscale(1)'}
  }

  observer.observe(element);
});



